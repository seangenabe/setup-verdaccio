param (
  [parameter(Position = 0)]
  [switch]
  $Revert
)

If ($Revert) {
  $env:npm_config_registry = ""
}
Else {
  $env:npm_config_registry = "http://localhost:4873/"
}
